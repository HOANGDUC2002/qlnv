function NhanVien(taiKhoan, name, email, matKhau, date, luong, chucVu, time) {
  this.taiKhoan = taiKhoan;
  this.name = name;
  this.email = email;
  this.matKhau = matKhau;
  this.date = date;
  this.luong = luong;
  this.chucVu = chucVu;
  this.time = time;
  this.sumLuong = function () {
    var result = 0;
    if (this.chucVu == "Sếp") {
      result += this.luong * 1 * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      result += this.luong * 1 * 2;
    } else if (this.chucVu == "Nhân viên") {
      result += this.luong * 1;
    } else {
      return;
    }
    result = new Intl.NumberFormat().format(result);
    return `${result} VND`;
  };
  this.Type = function () {
    var result = "";
    if (this.time >= 192) {
      result += "Xuất sắc";
    } else if (this.time >= 176 && this.time < 192) {
      result += " Giỏi";
    } else if (this.time >= 160 && this.time < 176) {
      result += " Khá";
    } else {
      result += " Trung bình";
    }
    return result;
  };
}
