function layThongTinTrenForm() {
  var taiKhoan = document.getElementById("tknv").value.trim();
  var name = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value.trim();
  var date = document.getElementById("datepicker").value;
  var luong = document.getElementById("luongCB").value.trim();
  var chucVu = document.getElementById("chucvu").value;
  var time = document.getElementById("gioLam").value.trim();
  var nv = new NhanVien(
    taiKhoan,
    name,
    email,
    matKhau,
    date,
    luong,
    chucVu,
    time
  );

  return nv;
}

function renderNV(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var current = list[i];
    var content = `
     <tr>
           <td>${current.taiKhoan}</td>
           <td>${current.name}</td>
           <td>${current.email}</td>
           <td>${current.date}</td>
           <td>${current.chucVu}</td>
           <td>${current.sumLuong()}</td>
           <td>${current.Type()}</td>
           <td>
           <button onclick="xoaNV('${
             current.taiKhoan
           }')" class="btn btn-danger">Xóa</button>
           <button onclick="suaNV('${
             current.taiKhoan
           }')" data-toggle="modal" data-target="#myModal" class="btn btn-primary fix-js">Sửa</button>
           </td>
           </tr>`;
    contentHTML += content;
  }

  document.querySelector("#tableDanhSach").innerHTML = contentHTML;
}

function showInformations(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.pass;
  document.getElementById("datepicker").value = nv.date;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.time;
}

function resetForm() {
  document.getElementById("myForm").reset();
}
