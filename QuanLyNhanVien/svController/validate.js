function kiemTraRong(value, idErr) {
  if (value.length == 0) {
    document.getElementById(idErr).innerHTML = "Vui lòng không để trống";
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).style.display = "none";
    document.getElementById(idErr).innerHTML = "";
    return true;
  }
}
function kiemTraTaiKhoan(value, idErr) {
  if (value.length >= 4 && value.length < 6) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";

    return true;
  } else {
    document.getElementById(idErr).style.display = "block";

    document.getElementById(idErr).innerHTML =
      "Vui lòng nhập tối đa từ 4-6 ký tự";
    return false;
  }
}

function kiemTraTen(value, idErr) {
  var regexText = new RegExp("[A-zÀ-ÿ]");
  if (regexText.test(value)) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  } else {
    document.getElementById(idErr).innerHTML = "Trường này phải là chữ";
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}
function kiemTraEmail(value, idErr) {
  var re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (re.test(value)) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  } else {
    document.getElementById(idErr).innerHTML = "Email tồn tại";
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}
function kiemTraMatKhau(value, idErr) {
  var regularExpression = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9_])"
  );
  var result = regularExpression.test(value);
  if (result) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  } else {
    document.getElementById(
      idErr
    ).innerHTML = `Vui lòng nhập từ tối thiểu 6 - 10 ký tự bao gồm ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt`;
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}
function kiemTraDoDaiMK(value, idErr, min, max) {
  if (value.length >= min && value.length <= max) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  } else {
    document.getElementById(
      idErr
    ).innerHTML = `Vui lòng nhập từ ${min} - ${max} ký tự bao gồm ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt`;
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}

function kiemTraLuong(value, idErr, min, max) {
  var result = value * 1;

  if (result * 1 >= min && result * 1 <= max) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  } else {
    document.getElementById(
      idErr
    ).innerHTML = `Vui lòng nhập số tiền trong khoảng từ 1.000.000VNĐ - 20.000.000VNĐ`;
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}
function kiemTraChucVu(value, idErr) {
  if (value == "Chọn chức vụ") {
    document.getElementById(idErr).innerHTML = `Vui lòng chọn trường này`;
    document.getElementById(idErr).style.display = "block";
    return false;
  } else {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  }
}
function kiemTraTimeLamViec(value, idErr, min, max) {
  if (value >= min && value <= max) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  } else {
    document.getElementById(
      idErr
    ).innerHTML = `Số giờ làm từ ${min} - ${max} giờ . Bạn vui lòng nhập lại`;
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}

function kiemTraLapTK(account, idErr, listStaff) {
  var index = listStaff.findIndex(function (staff) {
    return staff.account == account;
  });
  if (index == -1) {
    document.getElementById(idErr).innerHTML = "";
    document.getElementById(idErr).style.display = "none";
    return true;
  } else {
    document.getElementById(idErr).innerHTML = `Tài khoản đã tồn tại`;
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}
