var dsnv = [];
var dataJson = localStorage.getItem("DSNV");
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);

  dsnv = dataRaw.map(function (item) {
    var nv = new NhanVien(
      item.taiKhoan,
      item.name,
      item.email,
      item.matKhau,
      item.date,
      item.luong,
      item.chucVu,
      item.time
    );
    return nv;
  });
  renderNV(dsnv);
}

function saveLocalStorage() {
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV", dsnvJson);
}

function themNV() {
  var newNV = layThongTinTrenForm();
  var isValid = true;
  isValid =
    isValid & kiemTraRong(newNV.taiKhoan, "tbTKNV") &&
    kiemTraTaiKhoan(newNV.taiKhoan, "tbTKNV", 4, 6) &&
    kiemTraLapTK(newNV.taiKhoan, "tbTKNV", dsnv);
  isValid =
    isValid & kiemTraRong(newNV.name, "tbTen") &&
    kiemTraTen(newNV.name, "tbTen");

  isValid =
    isValid & kiemTraRong(newNV.email, "tbEmail") &&
    kiemTraEmail(newNV.email, "tbEmail");
  isValid =
    isValid & kiemTraRong(newNV.matKhau, "tbMatKhau") &&
    kiemTraMatKhau(newNV.matKhau, "tbMatKhau") &&
    kiemTraDoDaiMK(newNV.matKhau, "tbMatKhau", 6, 10);
  isValid = isValid & kiemTraRong(newNV.date, "tbNgay");
  isValid =
    isValid & kiemTraRong(newNV.luong, "tbLuongCB") &&
    kiemTraLuong(newNV.luong, "tbLuongCB", 1000000, 20000000);
  isValid =
    isValid & kiemTraRong(newNV.chucVu, "tbChucVu") &&
    kiemTraChucVu(newNV.chucVu, "tbChucVu");
  isValid =
    isValid & kiemTraRong(newNV.time, "tbGiolam") &&
    kiemTraTimeLamViec(newNV.time, "tbGiolam", 80, 200);
  if (!isValid) return;

  dsnv.push(newNV);
  saveLocalStorage();
  renderNV(dsnv);
}

function xoaNV(taiKhoan) {
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == taiKhoan;
  });

  if (index == -1) {
    return;
  }
  dsnv.splice(index, 1);
  saveLocalStorage();
  renderNV(dsnv);
}
function suaNV(taiKhoan) {
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == taiKhoan;
  });
  if (index == -1) {
    return;
  }

  var nv = dsnv[index];
  showInformations(nv);
  document.getElementById("tknv").disabled = true;
}
function capNhatNV() {
  var editNV = layThongTinTrenForm();
  console.log(editNV.taiKhoan);
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == editNV.taiKhoan;
  });
  console.log(index);
  if (index == -1) {
    return;
  }
  dsnv[index] = editNV;
  saveLocalStorage();
  renderNV(dsnv);
  document.getElementById("tknv").disabled = false;
}
function searchType() {
  var search = document.querySelector("#searchName").value;

  var newDSNV = dsnv.filter(function (nv) {
    return nv.Type().toUpperCase().includes(search.toUpperCase());
  });
  renderNV(newDSNV);
}
